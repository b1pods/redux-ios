import Logger

public protocol State: Equatable {
    init()
}

public protocol Action: CustomStringConvertible {}

extension Action where Self: RawRepresentable, Self.RawValue == String {
    public var description: String {
        return "\(String(describing: Self.self)).\(rawValue)"
    }
}

public typealias Reducer<S: State> = (_ action: Action, _ state: S) -> S

public protocol Reduceable: State {
    static func reduce(_ action: Action, _ state: Self) -> Self
}

open class Middleware<S: State> {
    public typealias Handler = (Action, @escaping () -> S, @escaping (Action) -> Void) -> Void
    
    private let handler: Handler?
    
    public init() {
        self.handler = nil
    }
    
    public init(with handler: @escaping Handler) {
        self.handler = handler
    }
    
    open func handle(_ action: Action,
                     currentState: @escaping () -> S,
                     dispatch: @escaping (Action) -> Void) {
        handler?(action, currentState, dispatch)
    }
}

public enum AlertState: State {
    case closed
    case presented

    public init() {
        self = .closed
    }
}

/// State that indicates asynchronous operation
public enum AsyncState<Result: Equatable>: State {
    /// Not initialized, empty
    case none
    /// Preparing process
    case preparing
    /// Common loading. Needed if there's no progress indicator
    case loading
    /// Indicates loading with finish percentage
    case progress(Double)
    /// Error occured
    case fail(Error)
    /// Success with result
    case success(Result)

    public init() {
        self = .none
    }

    public static func == (lhs: Redux_iOS.AsyncState<Result>, rhs: Redux_iOS.AsyncState<Result>) -> Bool {
        switch (lhs, rhs) {
        case (.none, .none): return true
        case (.preparing, .preparing): return true
        case (.loading, .loading): return true
        case let (.progress(lval), .progress(rval)) where lval == rval: return true
        case let (.success(lval), .success(rval)) where lval == rval: return true
        default: return false
        }
    }
}

public struct LogAction: Action {
    public let category: LoggerCategory?
    public let logType: LogType
    public let message: String
    public let file: String
    public let line: Int

    public init(category: LoggerCategory? = nil, logType: LogType, message: String, file: String = #fileID, line: Int = #line) {
        self.category = category
        self.logType = logType
        self.message = message
        self.file = file
        self.line = line
    }

    static public func debug(_ message: String, category: LoggerCategory? = nil, file: String = #fileID, line: Int = #line) -> LogAction {
        LogAction(category: category, logType: .debug, message: message, file: file, line: line)
    }

    static public func info(_ message: String, category: LoggerCategory? = nil, file: String = #fileID, line: Int = #line) -> LogAction {
        LogAction(category: category, logType: .info, message: message, file: file, line: line)
    }

    static public func `default`(_ message: String, category: LoggerCategory? = nil, file: String = #fileID, line: Int = #line) -> LogAction {
        LogAction(category: category, logType: .default, message: message, file: file, line: line)
    }

    static public func error(_ message: String, category: LoggerCategory? = nil, file: String = #fileID, line: Int = #line) -> LogAction {
        LogAction(category: category, logType: .error, message: message, file: file, line: line)
    }

    static public func fault(_ message: String, category: LoggerCategory? = nil, file: String = #fileID, line: Int = #line) -> LogAction {
        LogAction(category: category, logType: .fault, message: message, file: file, line: line)
    }
}

extension LogAction {
    public var description: String {
        return String(describing: self)
    }
}
