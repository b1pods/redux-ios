import Foundation
import Combine

public final class Store<S: State>: ObservableObject {
    private let reducer: Reducer<S>
    private var middlewares: [Middleware<S>]
    private let operationQueue: OperationQueue
    
    @Published public private(set) var state: S
    
    public init(with reducer: @escaping Reducer<S>, initialState: S, middlewares: [Middleware<S>] = []) {
        self.reducer = reducer
        self.state = initialState
        self.middlewares = middlewares
        
        self.operationQueue = OperationQueue()
        self.operationQueue.maxConcurrentOperationCount = 1
        self.operationQueue.qualityOfService = .userInitiated
    }
    
    convenience public init(_ initialState: S, middlewares: [Middleware<S>] = []) where S: Reduceable {
        self.init(with: S.reduce, initialState: initialState, middlewares: middlewares)
    }

    /// Adds action to dispatching queue
    /// - Parameter action: Dispatching action
    public func dispatch(_ action: Action) {
        self.operationQueue.addOperation { [weak self] in
            self?.handle(action, sendToMiddlewares: true)
        }
    }

    /// Dispatches action right now on main thread
    ///
    /// This method is needed to call simple actions without side effects
    /// - Parameter action: Dispatching action
    public func dispatchSimple(_ action: Action) {
        if Thread.isMainThread {
            handle(action, sendToMiddlewares: false)
        } else {
            DispatchQueue.main.sync {
                handle(action, sendToMiddlewares: false)
            }
        }
    }

    public func append(_ middleware: Middleware<S>) {
        middlewares.append(middleware)
    }

    public func append(_ middlewares: [Middleware<S>]) {
        self.middlewares.append(contentsOf: middlewares)
    }
    
    private func handle(_ action: Action, sendToMiddlewares: Bool) {
        let dispatch: (Action) -> Void = { [weak self] action in
            self?.dispatch(action)
        }

        let state = reducer(action, state)
        if Thread.isMainThread {
            self.state = state
        } else {
            DispatchQueue.main.sync {
                self.state = state
            }
        }
        if sendToMiddlewares {
            middlewares.forEach {
                $0.handle(action, currentState: { state }, dispatch: dispatch)
            }
        }
    }
}
