//
//  LogMiddleware.swift
//  Redux
//
//  Created by Дмитрий Шелонин on 12.09.2021.
//

import Logger

/// Options to indicate what may be printed
public struct LogMiddlewareOption: OptionSet {
    public let rawValue: UInt32

    public init(rawValue: UInt32) {
        self.rawValue = rawValue
    }

    public static let printAction = LogMiddlewareOption([])
    public static let printState = LogMiddlewareOption(rawValue: 1)
}

public final class LogMiddleware<S: State>: Middleware<S> {
    private static var defaultLoggerCategory: LoggerCategory { "Dispatch Action" }
    private let logger: LoggerEngine
    private let options: LogMiddlewareOption

    public init(options: LogMiddlewareOption = [.printState, .printAction], _ loggerEngine: LoggerEngine) {
        self.logger = loggerEngine
        self.options = options

        super.init()
    }

    public init(options: LogMiddlewareOption) {
        self.logger = ConsoleLogger(defaultCategory: Self.defaultLoggerCategory, options: [])
        self.options = options

        super.init()
    }

    public override init() {
        self.logger = ConsoleLogger(defaultCategory: Self.defaultLoggerCategory, options: [])
        self.options = [.printState, .printAction]

        super.init()
    }
    
    public override func handle(_ action: Action, currentState: @escaping () -> S, dispatch: @escaping (Action) -> Void) {
        let state = currentState()
        switch action {
        case let action as LogAction: handle(action, state: state)
        default: handleOther(action, state: state)
        }
    }

    private func handle(_ action: LogAction, state: S) {
        logger.write(group: action.category, action.message, logType: action.logType,
                     action.file, action.line)
    }

    private func handleOther(_ action: Action, state: S) {
        var items: [String] = []
        if options.contains(.printAction) {
            items.append("\(action)")
        }
        if options.contains(.printState) {
            items.append("\(state)")
        }
        let message = items.isEmpty ? "<undefined>" : items.joined(separator: "\n")
        logger.write(message, logType: .debug)
    }
}

extension Middleware {
    public static var logger: Middleware<S> {
        return LogMiddleware()
    }

    public static func logger(options: LogMiddlewareOption) -> Middleware<S> {
        return LogMiddleware(options: options)
    }
}
